resource "aws_launch_configuration" "launch_configuration" {
  count = var.create_launch_configuration ? 1 : 0
  name   =   var.launch_configuration_name
  image_id      = var.image_id
  instance_type = var.instance_type
  ebs_optimized = var.ebs_optimized
  enable_monitoring = var.enable_monitoring
  //key_name = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.allow-ssh[0].id]

  lifecycle {
    create_before_destroy = true
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      iops                  = lookup(ebs_block_device.value, "iops", 100)
      no_device             = lookup(ebs_block_device.value, "no_device", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", 8)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_device
    content {
      device_name  = ephemeral_block_device.value.device_name
      virtual_name = ephemeral_block_device.value.virtual_name
    }
  }

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
    }
  }
}


//AWS ASG tells about the min and max number of instances to be there initially

resource "aws_autoscaling_group" "AWS_ASG" {
  count = var.create_asg_group? 1 : 0
  name                 = var.ASG_Group_Name
  vpc_zone_identifier =  var.subnets 
  launch_configuration = aws_launch_configuration.launch_configuration[0].name
  min_size             = var.Minimum_Instances
  max_size             = var.Maximum_Instances
  desired_capacity     = var.desired_capacity
  health_check_type    = var.health_check_type
  health_check_grace_period = var. health_check_grace_period
  metrics_granularity =  var.metrics_granularity
  termination_policies = var.termination_policies
  enabled_metrics =  var.enabled_metrics
  wait_for_capacity_timeout = var.wait_for_capacity_timeout
  wait_for_elb_capacity =  var.wait_for_elb_capacity
  protect_from_scale_in     = var.protect_from_scale_in
  max_instance_lifetime = var.max_instance_lifetime
  suspended_processes       = var.suspended_processes
  lifecycle {
    create_before_destroy = true
  }
}

//Attaching ELB to an ASG

resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  count = var.create_asg_attachment ? 1 : 0
  autoscaling_group_name = aws_autoscaling_group.AWS_ASG[0].id
  elb                    = var.ELB
}

//Time at which the scaling actions are scheduled

resource "aws_autoscaling_schedule" "ASG_Schedule" {
  count = var.create_asg_schedule ? 1 : 0
  scheduled_action_name  = var.scheduled_action_name
  min_size               = var.Minimum_Instances
  max_size               = var.Maximum_Instances
  desired_capacity       = var.desired_capacity
  start_time             =  var.start_time
  end_time               =  var.end_time  
  autoscaling_group_name = aws_autoscaling_group.AWS_ASG[0].id
}

// Creating an SQS Queue


/*resource "aws_sqs_queue" "ASG_SQS_Queue" {
  count = var.create_SQS_Queue ? 1 : 0
  name =  var.SQS_Queue
}*/

// Life cycle hook is used to perform custom actions

resource "aws_autoscaling_lifecycle_hook" "ASG_Hook" {
    count = var.create_asg_with_initial_lifecycle_hook ? 1 : 0
    name                 =  var.lifecyclehook
    autoscaling_group_name = aws_autoscaling_group.AWS_ASG[0].id
    default_result       =  var.hook_default_result
    heartbeat_timeout    =  var.hook_heartbeat_timeout 
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
    notification_target_arn = "arn:aws:sqs:ap-south-1:210339272200:sqsqueue"
    role_arn                = "arn:aws:iam::210339272200:role/ASG_Notification_Role"
    //depends_on  =  [aws_iam_role.ASG_Notification_Role,]
}

// Creating an SNS Topic

/*resource "aws_sns_topic" "ASG_SNS_Topic" {
  count = var.create_SNS_Topic ? 1 : 0
  name =  var.SNS_Topic
}*/

//Sending a notification to SNS if there is an update on the instances

resource "aws_autoscaling_notification" "ASG_Notification" {
  count = var.create_SNS_Notification ? 1 : 0
   group_names = [
    aws_autoscaling_group.AWS_ASG[0].id,
   ]
   notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR",
  ]
topic_arn = aws_sns_topic.ASG_SNS_Topic[0].arn
 }

//Endpoint for the topic to receive the notofications

//resource "aws_sns_topic_subscription" "user_updates_sqs_target" {
  //topic_arn = "arn:aws:sns:ap-south-1:432981146916:ASG_SNS_Topic"
  //protocol  = "Email"
  //endpoint  = "rajithapulavarthi555@gmail.com"
  //endpoint  = "arn:aws:sqs:ap-south-1:444455556666:ASG_SQS_Queue*"
//}

/*depends_on = [
    "aws_iam_role.ASG_Notification_Role"
  ]*/

// Adjustment of instances
resource "aws_autoscaling_policy" "scale-up" {
  count = var.create_asg_policy_up? 1 : 0
    name =  var.asg_policy_up_name
    scaling_adjustment = var.scaling_adjustment_up
    adjustment_type =  var.adjustment_type
    cooldown = var.cooldown
    autoscaling_group_name = "${aws_autoscaling_group.AWS_ASG[0].name}"
}

resource "aws_autoscaling_policy" "scale-down" {
  count = var.create_asg_policy_down ? 1 : 0
    name = var.asg_policy_down_name
    scaling_adjustment = var.scaling_adjustment_down
    adjustment_type = var.adjustment_type
    cooldown = var.cooldown
    autoscaling_group_name = "${aws_autoscaling_group.AWS_ASG[0].name}"
}

// Alarm  tells about at which time the new instance to be created or deleted
/*resource "aws_cloudwatch_metric_alarm" "scales-up" {
  count = var.create_metric_alarm_up ? 1 : 0
    alarm_name =  var.alarm_up_name
    comparison_operator =  var.comparison_operator_up
    evaluation_periods =  var.evaluation_periods
    metric_name =  var.metric_name
    namespace =  var.namespace
    period =  var.period
    statistic =  var.statistic
    threshold =  var.threshold_up
    alarm_description = var.alarm_description
    alarm_actions = [aws_autoscaling_policy.scale-up[0].arn]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.AWS_ASG[0].name}"
    }
}

resource "aws_cloudwatch_metric_alarm" "scales_down" {
  count = var.create_metric_alarm_down ? 1 : 0
    alarm_name =  var.alarm_down_name
    comparison_operator =  var.comparison_operator_down
    evaluation_periods = var.evaluation_periods
    metric_name =  var.metric_name
    namespace = var.namespace
    period = var.period
    statistic =  var.statistic
    threshold =  var.threshold_down
    alarm_description = var.alarm_description
    alarm_actions = [aws_autoscaling_policy.scale-down[0].arn]
    dimensions = {
        AutoScalingGroupName = "${aws_autoscaling_group.AWS_ASG[0].name}"
    }
}

//AWS Security Group
resource "aws_security_group" "allow-ssh" {
  count = var.create_SG_ASG ? 1 : 0
  vpc_id      =  var.vpc_id
  name        =  var.securitygroup_name
  description = "security group that allows ssh and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow-ssh"
  }
}*/
