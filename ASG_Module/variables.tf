variable "image_id" {
   default =""
}

variable  "instance_type" {
     default = ""
}


variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
   default = ""
}

variable "enable_monitoring" {
  description = "Enables/disables detailed monitoring. This is enabled by default."
   default = ""
}

variable "root_block_device" {
  description = "Customize details about the root block device of the instance"
   default = []
}

variable "ebs_block_device" {
  description = "Additional EBS block devices to attach to the instance"
   default = []
}

variable "ephemeral_block_device" {
  description = "Customize Ephemeral (also known as 'Instance Store') volumes on the instance"
   default = []
}

variable "ASG_Group_Name" {
     default = ""  
}

variable "Minimum_Instances" {
     default = "" 
}

variable "Maximum_Instances" {
     default = ""  
}

variable "desired_capacity" {
     default = ""  
}

variable "health_check_type" {
     default = ""
}


variable "health_check_grace_period" {
    default = ""
}

variable "termination_policies" {
    default = ""
}

variable "metrics_granularity" {
    default = ""
}

variable "enabled_metrics" {
    default = []
}

variable "wait_for_capacity_timeout" {
    default = ""
}

variable "wait_for_elb_capacity" {
    default = ""
}

variable "protect_from_scale_in" {
  description = "Allows setting instance protection. The autoscaling group will not select instances with this setting for termination during scale in events."
   default = ""
}

variable "max_instance_lifetime" {
    default = ""
}

variable "suspended_processes" {
  description = "A list of processes to suspend for the AutoScaling Group. The allowed values are Launch, Terminate, HealthCheck, ReplaceUnhealthy, AZRebalance, AlarmNotification, ScheduledActions, AddToLoadBalancer. Note that if you suspend either the Launch or Terminate process types, it can prevent your autoscaling group from functioning properly."
   default = []
}

variable "ELB" {
   default = ""
}

variable "start_time" {
    default = ""
}

variable "end_time" {
    default = ""
}

variable "SQS_Queue" {
    default = ""
}

variable "lifecyclehook" {
    default = ""
}


variable "hook_default_result" {
    default = ""
}


variable "hook_heartbeat_timeout" {
    default = ""
}

variable "SNS_Topic" {
    default = ""
}

variable "asg_policy_up_name" {
    default = ""
}

variable "scaling_adjustment_up" {
    default = ""
}

variable "adjustment_type" {
    default = ""
}

variable "cooldown" {
    default = ""
}

variable "asg_policy_down_name" {
    default = ""
}

variable "scaling_adjustment_down" {
    default = ""
}

variable "alarm_up_name" {
    default = ""
}

variable "comparison_operator_up" {
    default = ""
}

variable "evaluation_periods" {
    default = ""
}

variable "namespace" {
    default = ""
}

variable "metric_name" {
    default = ""
}

variable "period" {
    default = ""
}

variable "statistic" {
    default = ""
}

variable "threshold_up" {
    default = ""
}

variable "alarm_description" {
    default = ""
}

variable "alarm_down_name" {
    default = ""
}

variable "comparison_operator_down" {
   default = ""
}

variable "threshold_down" {
    default = ""
}


variable "securitygroup_name" {
     default = "" 
}

variable "create_asg_with_initial_lifecycle_hook" {
  description = "Create an ASG with initial lifecycle hook"
  default = ""
}

variable "create_asg_schedule" {
  description = "Create an ASG with initial lifecycle hook"
   default = ""
}

variable "subnets" {
    default = ""
}

variable "vpc_id" {
    default = ""
}

variable "create_launch_configuration" {
   default = ""
}

variable "create_asg_group" {
    default = ""
}

variable "create_asg_attachment" {
    default = ""
}

variable "create_SQS_Queue" {
   type = bool
}

variable "create_SNS_Topic" {
   type = bool
}

variable "create_SNS_Notification" {
    default = ""
}

variable "create_asg_policy_up" {
    default = ""
}

variable "create_asg_policy_down" {
    default = ""
}

variable "create_metric_alarm_up" {
    default = ""
}

variable "create_metric_alarm_down" {
    default = ""
}

variable "create_SG_ASG" {
   type = bool
}

variable "scheduled_action_name" {
  default = "" 
}

variable "launch_configuration_name" {
  default = "" 
}

