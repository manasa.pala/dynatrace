
locals {
  is_t_instance_type = replace(var.instance_type, "/^t(2|3|3a){1}\\..*$/", "1") == "1" ? true : false
}


resource "aws_instance" "ec2" {
  count = var.instance_count ? 1:0

  ami              = var.ami
  instance_type    = var.instance_type
  user_data        =  var.user_data 

  # user_data_base64 = var.user_data_base64
  # subnet_id = length(var.network_interface) > 0 ? null : element(
  #   distinct(compact(concat([var.subnet_id], var.subnet_ids))),
  #   count.index,
  # )
  subnet_id = var.subnet_id
  availability_zone  = var.instance_availability_zone
  key_name               = var.key_name == null ? aws_key_pair.nile-key[0].key_name : var.key_name
  monitoring             = var.monitoring
  get_password_data      = var.get_password_data
  vpc_security_group_ids = var.sg_ids
  iam_instance_profile   = var.aws_iam_instance_profile_name

  associate_public_ip_address = var.associate_public_ip_address
  private_ip                  = length(var.private_ips) > 0 ? element(var.private_ips, count.index) : var.private_ip
  ipv6_address_count          = var.ipv6_address_count
  ipv6_addresses              = var.ipv6_addresses

  ebs_optimized = var.ebs_optimized

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      kms_key_id            = lookup(root_block_device.value, "kms_key_id", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      iops                  = lookup(ebs_block_device.value, "iops", null)
      kms_key_id            = lookup(ebs_block_device.value, "kms_key_id", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_device
    content {
      device_name  = ephemeral_block_device.value.device_name
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
    }
  }

  dynamic "network_interface" {
    for_each = var.network_interface
    content {
      device_index          = network_interface.value.device_index
      network_interface_id  = lookup(network_interface.value, "network_interface_id", null)
      delete_on_termination = lookup(network_interface.value, "delete_on_termination", false)
    }
  }

  source_dest_check                    = length(var.network_interface) > 0 ? null : var.source_dest_check
  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group == null ? aws_placement_group.ec2-placement_strategy[0].name : null
  tenancy                             = var.tenancy 

  tags = var.tags
  

  volume_tags =var.volume_tags
  

  credit_specification {
    cpu_credits = local.is_t_instance_type ? var.cpu_credits : null
  }
}


# output "ec2_instance_id" {
#   value = [for s in aws_instance.ec2: s.id]
# }

# output "ec2_instance_type" {
#   value = [for s in aws_instance.ec2: s.instance_type]
# }

# output "ebs_volume_id" {
# 	value = [for d in aws_instance.ec2: 
# 				[
# 					for m in d.ebs_block_device : m.volume_id
# 				]
# 			]	
# }

resource "aws_ami_from_instance" "ec2_ami" {
  count              =  var.ami_creation ? 1:0
  name               =   var.aws_ami_name 
  source_instance_id = aws_instance.ec2[0].id
}

## Provide launch permission to create aws ami from existing instance 

# resource "aws_ami_launch_permission" "example" {
#   image_id   = aws_ami_from_instance.ami[0].id
#   account_id = var.account_id
#   depends_on = [aws_ami_from_instance.ami]
# }


resource "aws_key_pair" "nile-key" {
  count      =  var.aws_key_pair_creation ? 1:0
  key_name   =   var.key_name 
  public_key =   var.public_key 
}

resource "aws_placement_group" "ec2-placement_strategy" {
  count      =  var.aws_placement_group_creation ? 1:0
  name    =    var.placement_group_name 
  strategy    = var.placement_group
}

# module "ec2_module" {
  
#   count  = ! var.fleet_enabled && ! var.eip_enabled && ! var.ebs_snapshot && ! var.autoscaling ? 1 : 0
#   source = "./ec2_core"
# }

locals {

  is_autoscaling_enabled               = var.autoscaling && ! var.eip_enabled && ! var.ebs_snapshot ? true : false
  is_ebs_enabled                       = var.ebs_snapshot && ! var.eip_enabled && ! var.autoscaling ? true : false
  is_eip_enabled                       = var.eip_enabled && ! var.ebs_snapshot && ! var.autoscaling ? true : false
  is_autoscale_and_eip_and_ebs_enabled = var.autoscaling && var.ebs_snapshot && var.eip_enabled ? true : false
  is_autoscale_and_eip_enabled         = var.autoscaling && var.eip_enabled && ! var.ebs_snapshot ? true : false
  is_autoscale_and_ebs_enabled         = var.autoscaling && var.ebs_snapshot && ! var.eip_enabled ? true : false
  is_ebs_and_eip_enabled               = var.ebs_snapshot && var.eip_enabled && ! var.autoscaling ? true : false
}

resource "aws_eip" "nile_eip" {

  count    = local.is_eip_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_ebs_and_eip_enabled ? 1 : 0
  //instance = element(module.ec2_mod[0].ec2_instance_id, count.index)
  vpc      = var.aws_eip_vpc


  tags = {
    Name = var.aws_eip_tag_name 
  }
}

## Provides an AWS EIP Association as a top level resource, to associate and disassociate Elastic IPs from AWS Instances and Network Interfaces ##

resource "aws_eip_association" "nile_eip_association" {
  count               = local.is_eip_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_ebs_and_eip_enabled ? 1 : 0
  instance_id         = aws_instance.ec2[0].id
  allocation_id       = aws_eip.nile_eip[0].id
  allow_reassociation = var.Allow_Reassociation
  #   network_interface_id  = aws_network_interface.test.id
  #   private_ip_address    = var.EIP_Associated_Private_IP
}

resource "aws_ebs_volume" "example" {
  count     = var.ebs_volume_creation ? 1:0
  availability_zone =   var.availability_zone 
  size              = var.volume_size
  encrypted          = var.encrypted
  #iops               = var.iops
  multi_attach_enabled  = var.multi_attach_enabled
  snapshot_id     = var.snapshot_id
  outpost_arn  = var.outpost_arn
  type         = var.type 
  kms_key_id    = var.kms_key_id

  tags = {
    Name = var.tag_volume
  }
}

resource "aws_volume_attachment" "ebs_att" {
  count  = var.volume_attachment_creation ? 1 :0
  device_name =  var.device_name 
  volume_id   = aws_ebs_volume.example[0].id
  instance_id = aws_instance.ec2[0].id
  force_detach  = var.force_detach
  skip_destroy   = var.skip_destroy
}

resource "aws_snapshot_create_volume_permission" "example_perm" {
  count  = var.snapshot_create_volume_permission ? 1:0
  snapshot_id = aws_ebs_snapshot.nile-ec2-snapshot[0].id
  account_id  = var.account_id
}

resource "aws_ebs_snapshot" "nile-ec2-snapshot" {

  //count = local.is_ebs_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_ebs_enabled || local.is_ebs_and_eip_enabled ? 1 : 0
  count  = var.ebs_snapshot_creation ? 1:0
  volume_id   =  var.vol_ids[count.index]
  //volume_id   = element(module.ec2_mod[0].ebs_volume_id[0], 0)
  description = var.snapshot_description

  tags = {
    Name = var.tag_name_snap
  }

  # timeouts{
  #   create            = var.timeouts_create
  #   delete            = var.timeouts_delete
  # }

  # depends_on = []
}

resource "aws_ebs_snapshot_copy" "example_copy" {
  count     = var.snapshot_copy_creation ? 1: 0
  source_snapshot_id = aws_ebs_snapshot.nile-ec2-snapshot[0].id
  source_region      = var.snapshot_region

  tags = {
    Name = var.tag_name_snap_copy
  }
}


resource "aws_launch_configuration" "as_conf" {

  count = local.is_autoscaling_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_autoscale_and_ebs_enabled ? 1 : 0
  name = var.aws_autoscaling_launc_config_name
  #name_prefix    = var.aws_launch_configuration_name_prefix
  image_id      = aws_ami_from_instance.ami[0].id
  instance_type =   var.launch_config_instance_type 
  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_ec2_availability_zone_group" "example" {
  count   = var.aws_ec2_availability_zone_group_creation ? 1:0
  group_name    =   var.azg_name 
  opt_in_status =    var.azg_status 
}

## Create an AMI out of the ec2 instance created

resource "aws_ami_from_instance" "ami" {
  count              = local.is_autoscaling_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_autoscale_and_ebs_enabled ? 1 : 0
  name               =  var.aws_ami_from_inst_name 
  source_instance_id = aws_instance.ec2[0].id
}

## Provide launch permission to create aws ami from existing instance 

# resource "aws_ami_launch_permission" "example" {
#   image_id   = aws_ami_from_instance.ami[0].id
#   account_id = data.aws_caller_identity.current.account_id
#   depends_on = [aws_ami_from_instance.ami]
# }

## Used for autoscaling groups ##

resource "aws_autoscaling_group" "niledemo" {

  count = local.is_autoscaling_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_autoscale_and_ebs_enabled ? 1 : 0
  name  = var.aws_autoscaling_group_name
  launch_configuration = aws_launch_configuration.as_conf[0].name
  vpc_zone_identifier  = [var.subnet_id1]
  min_size             = var.aws_autoscaling_group_minsize
  max_size             = var.aws_autoscaling_group_maxsize
  
  lifecycle {
    create_before_destroy = true
  }
}


# Create a new ALB Target Group attachment
resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  count                  = local.is_autoscaling_enabled || local.is_autoscale_and_eip_and_ebs_enabled || local.is_autoscale_and_eip_enabled || local.is_autoscale_and_ebs_enabled ? 1 : 0
  autoscaling_group_name = aws_autoscaling_group.niledemo[0].id
  alb_target_group_arn   = var.alb_arn
}

# module "ec2_mod" {
#   count  = var.autoscaling || var.ebs_snapshot || var.eip_enabled ? 1 : 0
#   source = "./ec2_core"

# }

resource "aws_ebs_default_kms_key" "example" {
  count    = var.kms_key_creation ? 1:0 
  key_arn = var.aws_kms_key
}

resource "aws_ebs_encryption_by_default" "example" {
  count    = var.kms_key_encryption_creation ? 1:0 
  enabled =  var.encryption_enabled 
}

resource "aws_ec2_fleet" "vg_ec2_fleet" {
  count    = var.ec2_fleet_creation ? 1:0 
  
  launch_template_config {
    launch_template_specification {
    #   launch_template_id = "lt-0bd6ecc75445e53cb"
      launch_template_id = aws_launch_template.launch-template[0].id
      # launch_template_id = data.aws_launch_template.default.id
      # version           = data.aws_launch_template.default.latest_version
      version            = aws_launch_template.launch-template[0].latest_version
    }
  }

  target_capacity_specification {
    default_target_capacity_type = var.default_target_capacity_type
    total_target_capacity        = var.total_target_capacity
  }

  excess_capacity_termination_policy = var.excess_capacity_termination_policy
  on_demand_options{
	allocation_strategy = var.allocation_strategy
  }
  
  replace_unhealthy_instances = var.replace_unhealthy_instances
  
  spot_options{
    allocation_strategy = var.allocation_strategy
    instance_interruption_behavior = var.spot_options_instance_interruption_behavior
    instance_pools_to_use_count = var.instance_pools_to_use_count
  }
  terminate_instances = var.terminate_instances
  
  terminate_instances_with_expiration = var.terminate_instances_with_expiration
  type = var.fleet_type
  
  tags = {
	  Name = var.tag_ec2_fleet
  }

}



resource "aws_launch_template" "launch-template" {
  count    = var.launch_template_creation ? 1:0
  name = var.template_name

  block_device_mappings {
    device_name = var.block_device_name

    ebs {
      volume_size = var.ebs_size
    }
  }

  capacity_reservation_specification {
    capacity_reservation_preference = var.capacity_reservation_preference
  }

  # cpu_options {
  #   core_count       = var.core_count
  #   threads_per_core = var.threads_per_core
  # }

  credit_specification {
    cpu_credits = var.credit_specification_cpu_credits
  }

  disable_api_termination = var.launch_template_disable_api_termination

  ebs_optimized = var.launch_template_ebs_optimized


  elastic_gpu_specifications {
    type = var.elastic_gpu_specifications_type
  }

  elastic_inference_accelerator {
    type = var.elastic_inference_accelerator_type
  }

  iam_instance_profile {
    # arn = aws_iam_instance_profile.iam_instance_profile.arn
     name = var.aws_iam_instance_profile_name
  }

  image_id = var.image_id

  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior

  # instance_market_options {
  #   market_type = var.market_type
  # }

  instance_type = var.instance_type

  # kernel_id = "test"

  key_name = var.key_name

  # license_specification {
  #   license_configuration_arn = "arn:aws:license-manager:eu-west-1:123456789012:license-configuration:lic-0123456789abcdef0123456789abcdef"
  # }

  metadata_options {
    http_endpoint               = var.http_endpoint
    http_tokens                 = var.http_tokens
    http_put_response_hop_limit = var.http_put_response_hop_limit
  }
  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = var.launch_template_associate_public_ip_address
  }

  placement {
    availability_zone =  var.placement_availability_zone
   
  }

  # ram_disk_id = "test"

  vpc_security_group_ids = var.sg_ids

  tag_specifications {
    resource_type = var.resource_type

    tags = {
      Name = var.tag_specifications_tagname
    }
  }

  # user_data = filebase64("${path.module}/example.sh")
}


resource "aws_ec2_capacity_reservation" "nile-capacity-reservation" {
  count    = var.capacity_reservation_creation ? 1:0
  instance_type     = var.instance_type
  instance_platform = var.instance_platform
  availability_zone = var.placement_availability_zone
  instance_count    = var.instance_count_create
  ebs_optimized     = var.capacity_reservation_ebs_optimized
  end_date          = var.end_date
  end_date_type     = var.end_date_type
  ephemeral_storage = var.ephemeral_storage
  instance_match_criteria = var.instance_match_criteria
  tenancy           = var.capacity_reservation_tenancy 

}




