variable "vpc_id"{

}

variable "tag_name" {

}

variable "name" {
  description = "Name to be used on all resources as prefix"
  type        = string

}

variable "instance_count" {
  description = "Number of instances to launch"
  type        = bool

}

variable "instance_count_create" {
  description = "Number of instances to launch"
 

}

variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string

}

variable "get_password_data" {
  description = "If true, wait for password data to become available and retrieve it."
  type        = bool

}

variable "sg_ids" {
  type        = list

}

variable "aws_iam_instance_profile_name" {
  type        = string

}

variable "tenancy" {

  type        = string

}

variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
  type        = bool

}

variable "disable_api_termination" {
  description = "If true, enables EC2 Instance Termination Protection"
  type        = bool

}



variable "instance_type" {
  description = "The type of instance to start"
  type        = string

}

variable "monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled"
  type        = bool

}

variable "vpc_security_group_ids" {
  description = "A list of security group IDs to associate with"
  type        = list(string)

}

variable "subnet_id" {
  description = "The VPC Subnet ID to launch in"
  type        = string

}

variable "instance_availability_zone" {
  type        = string
}

# variable "subnet_ids" {
#   description = "A list of VPC Subnet IDs to launch in"
#   type        = list(string)

# }


variable "associate_public_ip_address" {
  description = "If true, the EC2 instance will have associated public IP address"
  type        = bool

}

variable "private_ip" {
  description = "Private IP address to associate with the instance in a VPC"
  type        = string

}

variable "private_ips" {
  description = "A list of private IP address to associate with the instance in a VPC. Should match the number of instances."
  type        = list(string)

}

variable "source_dest_check" {
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance. Used for NAT or VPNs."
  type        = bool

}

variable "user_data" {
  description = "The user data to provide when launching the instance. Do not pass gzip-compressed data via this argument; see user_data_base64 instead."
  type        = string
}

variable "user_data_base64" {
  description = "Can be used instead of user_data to pass base64-encoded binary data directly. Use this instead of user_data whenever the value is not a valid UTF-8 string. For example, gzip-encoded user data must be base64-encoded and passed via this argument to avoid corruption."
  type        = string

}

variable "iam_instance_profile" {
  description = "The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile."
  type        = string

}

variable "ipv6_address_count" {
  description = "A number of IPv6 addresses to associate with the primary network interface. Amazon EC2 chooses the IPv6 addresses from the range of your subnet."
  type        = number

}

variable "ipv6_addresses" {
  description = "Specify one or more IPv6 addresses from the range of the subnet to associate with the primary network interface"
  type        = list(string)

}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)

}

variable "volume_tags" {
  description = "A mapping of tags to assign to the devices created by the instance at launch time"
  type        = map(string)

}

variable "root_block_device" {
  description = "Customize details about the root block device of the instance. See Block Devices below for details"
  type        = list(map(string))
}

variable "ebs_block_device" {
  description = "Additional EBS block devices to attach to the instance"
  type        = list(map(string))

}

# variable "ebs_block_device" {
#   description = "Additional EBS block devices to attach to the instance"
#   type        = list(map(string))

# }



variable "ephemeral_block_device" {
  description = "Customize Ephemeral (also known as Instance Store) volumes on the instance"
  type        = list(map(string))

}

variable "network_interface" {
  description = "Customize network interfaces to be attached at instance boot time"
  type        = list(map(string))

}

variable "cpu_credits" {
  description = "The credit option for CPU usage (unlimited or standard)"
  type        = string

}

variable "use_num_suffix" {
  description = "Always append numerical suffix to instance name, even if instance_count is 1"
  type        = bool

}

variable "num_suffix_format" {
  description = "Numerical suffix format used as the volume and EC2 instance name suffix"
  type        = string

}

variable "aws_key_pair_creation" {
    type = bool

}

variable "key_name" {
    type = string

}

variable "public_key" {
    type = string

}


variable "aws_placement_group_creation" {
    type = bool

}

variable "placement_group_name" {
    type = string

}

variable "placement_group" {
    type = string

}

variable "ami_creation" {
    type = bool

}

variable "aws_ami_name" {
    type = string

}



variable "subnet_id1" {
  description = "The VPC Subnet ID to launch in"
  type        = string

}

variable "subnet_ids" {
  description = "A list of VPC Subnet IDs to launch in"
  type        = list(string)

}

variable "aws_autoscaling_group_name" {

}

variable "aws_autoscaling_launc_config_name" {

}

variable "launch_config_instance_type" {
  type        = string

}

variable "aws_ec2_availability_zone_group_creation" {
  type        = bool

}

variable "azg_name" {
  type        = string

}

variable "azg_status" {
  type        = string

}


variable "create_before_destroy" {
  type        = bool

}


variable "aws_ami_from_inst_name" {
  type        = string

}

variable "aws_autoscaling_group_maxsize" {

}

variable "aws_autoscaling_group_minsize" {

}

variable "aws_launch_configuration_name_prefix" {

}

variable "lifecycle_create_before_destroy" {

}

variable "autoscaling" {
    type = bool

}

variable "alb_arn" {

}


## For EBS snapshot:

variable "vol_ids" {
  type = list

}

variable "ebs_snapshot" {
    type = bool

}

variable "account_id" {
    type = string

}

variable "snapshot_create_volume_permission" {
    type = bool

}

variable "ebs_snapshot_creation" {
    type = bool

}

variable "snapshot_description" {

}

variable "timeouts_create" {

}

variable "timeouts_update" {

}

variable "timeouts_delete" {

}

variable "snapshot_copy_creation" {
  type  = bool

}


variable "snapshot_region" {

}

variable "snapshot_encrypted" {

}

variable "tag_name_snap_copy" {
  type    = string

}

variable "tag_name_snap" {
  type    = string

}

variable "ebs_volume_creation" {
  type    = bool

}

variable "availability_zone" {
  type    = string

}

variable "volume_size" {
  type  = number

}

variable "encrypted" {
  type    = bool

}

variable "iops" {
  type    = number
}

variable "multi_attach_enabled" {
  type    = bool
  
}

variable "snapshot_id" {
  type    = string
}

variable "outpost_arn" {
  type    = string
}

variable "type" {
  type    = string
}

variable "kms_key_id" {
  type    = string
}

variable "tag_volume" {
  type    = string

}

variable "volume_attachment_creation" {}

variable "device_name" {
  type    = string

}

variable "force_detach" {}

variable "skip_destroy" {}





## For EIP and Association

variable "eip_enabled" {
    type = bool

}

variable "EIP_Associated_Private_IP" {

}

variable "Public_IPV4_Pool" {

}

variable "Customer_Owned_IPV4_Pool" {

}

variable "Allow_Reassociation" {
  type = bool

}

variable "aws_eip_vpc" {
  type = bool

}

variable "aws_eip_tag_name" {
  type   = string

}


variable "aws_kms_key" {
  type   = string

}

variable "encryption_enabled" {
  type   = bool
}

variable "kms_key_creation" {
  type   = bool
}

variable "kms_key_encryption_creation" {
  type   = bool
}

###########################################



variable "launch_template_creation" {
	type = bool
}

variable "ec2_fleet_creation" {
	type = bool
}

variable "template_name" {

}

variable "launch_template_description" {

}

variable "block_device_name" {

}


variable "ebs_size" {

}

variable "capacity_reservation_preference" {


}

variable "core_count" {

}

variable "threads_per_core" {

}

variable "credit_specification_cpu_credits" {

}

variable "launch_template_disable_api_termination" {

}

variable "launch_template_ebs_optimized" {

}

variable "elastic_gpu_specifications_type" {

}

variable "elastic_inference_accelerator_type" {

}

variable "instance_profile_arn" {

}

variable "iam_instance_profile_name" {

}

variable "image_id" {

}

variable "instance_initiated_shutdown_behavior" {

}

variable "market_type" {

}

variable "block_duration_minutes" {

}

variable "instance_interruption_behavior" {

}

variable "max_price" {

}

variable "spot_instance_type" {

}

variable "valid_until" {

}


variable "kernel_id" {

}

variable "launch_template_key_name" {

}

variable "license_configuration_arn" {

}

variable "http_endpoint" {

}

variable "http_tokens" {

}

variable "http_put_response_hop_limit" {

}

variable "monitoring_enabled" {

}

variable "placement_availability_zone" {

}
variable "ram_disk_id" {

}

variable "resource_type" {

}
variable "tag_specifications_tagname" {

}
variable "launch_template_user_data" {

}

variable "hibernation_options" {

}

variable "launch_template_associate_public_ip_address" {

}



variable "network_delete_on_termination" {
  type = bool

}


variable "instance_platform" {

} 

variable "capacity_reservation_creation" {
  type        = bool

}

variable "capacity_reservation_ebs_optimized" {

} 
variable "end_date" {

} 
variable "end_date_type" {

} 
variable "ephemeral_storage" {

} 
variable "instance_match_criteria" {

} 
variable "capacity_reservation_tenancy" {}


variable "excess_capacity_termination_policy" {

}
variable "allocation_strategy" {

}

variable "replace_unhealthy_instances" {

}

variable "spot_options_instance_interruption_behavior" {

}
variable "instance_pools_to_use_count" {

}

variable "terminate_instances" {

}

variable "terminate_instances_with_expiration" {

}

variable "fleet_type" {

}
variable "tag_ec2_fleet" {

}

variable "total_target_capacity" {

}

variable "default_target_capacity_type" {

}






